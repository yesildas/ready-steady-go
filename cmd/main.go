package main

import (
	"fmt"
	"gitlab.reutlingen-university.de/yesildas/health"
)

func main() {
	echo := health.Echo("Ready steady go!")
	fmt.Println(echo)
}
